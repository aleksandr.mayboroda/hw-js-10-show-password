let formBlock = document.querySelector('.password-form')
if(formBlock)
{
    formBlock.addEventListener('click', function(event) {
        // смена типа инпута и иконки
        if(event.target.classList.contains('icon-password'))
        {
            if(event.target.classList.contains('fa-eye'))
            {
            event.target.className = event.target.className.replace('fa-eye','fa-eye-slash')
            event.target.parentElement.querySelector('input').type = 'text'
            }
            else
            {
            event.target.className = event.target.className.replace('fa-eye-slash','fa-eye')
            event.target.parentElement.querySelector('input').type = 'password'
            }
        }
        
        //submit form (чтобы не плодить отдельный слушатель) - не лучший способ, но мне было лень!
        if(event.target.tagName == 'BUTTON', event.target.type === "submit")
        {
            event.preventDefault()
            let inputsArr = event.currentTarget.querySelectorAll('input')
            if(inputsArr.length > 0)
            {
                inputsArr = Array.from(event.currentTarget.querySelectorAll('input'))

                // проверка, чтобы поля были === и !== ''
                if(inputsArr.reduce((a,b) => a.value === b.value) && inputsArr.every(elem => elem.value !== ''))
                {
                    // удаляем ошибку
                    toggleError(inputsArr[inputsArr.length - 1])
                    alert('You are welcome')
                }
                else
                {
                    toggleError(inputsArr[inputsArr.length - 1],'Нужно ввести одинаковые значения!')
                }

            }

        }
    })
}

// отображение/удаление ошибки
function toggleError(elem,text = '') {
    let errorElem = elem.parentElement.querySelector('.form-error'),
        newError
    if(errorElem)
    {
        errorElem.remove()
    }

    if(text)
    {
        elem.insertAdjacentHTML('afterend',`<p class="form-error">${text}</p>`)
        // newError = document.createElement('p')
        // newError.className = 'form-error'
        // newError.innerText = text
        // elem.after(newError)
    }
}
